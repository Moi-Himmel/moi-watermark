package cn.moi.util.watermark.config;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;

/**
 * 类说明：IconWatermarkConfig
 *
 * @author Moi.Himmel
 * @since 2023/10/17
 **/
@Data
@EqualsAndHashCode(callSuper = true)
public class IconWatermarkConfig extends AbstractWatermarkConfig {

    @Serial
    private static final long serialVersionUID = 2473336406373273959L;

    /**
     * 水印图片缩放比例
     */
    private Double zoom;

}
