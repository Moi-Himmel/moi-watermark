package cn.moi.util.watermark.config;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.util.List;

/**
 * 类说明：TextWatermarkConfig
 *
 * @author Moi.Himmel
 * @since 2023/10/17
 **/
@Data
@EqualsAndHashCode(callSuper = true)
public class TextWatermarkConfig extends AbstractWatermarkConfig {

    @Serial
    private static final long serialVersionUID = 2604738212536226956L;

    /**
     * 文字内容
     */
    private String text;

    /**
     * 是否需要添加额外信息
     */
    private String extra;

    /**
     * 字体文件名，和 fontPath 一起用于 PDF 添加水印时读取字体文件
     */
    private String fontFileName;

    /**
     * 字体名称
     */
    private String fontName;

    /**
     * 字体文件路径，示例："C:\\{userHome}\\AppData\\Local\\Microsoft\\Windows\\Fonts\\"
     * 若此字段为空，则取系统默认字体文件存储路径
     */
    private String fontPath;

    /**
     * 字体大小
     */
    private Integer fontSize;

    /**
     * 字体颜色
     */
    private List<Integer> color;

}
