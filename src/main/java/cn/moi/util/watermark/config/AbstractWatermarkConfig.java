package cn.moi.util.watermark.config;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 类说明：AbstractWatermarkConfig
 *
 * @author Moi.Himmel
 * @since 2023/10/17
 **/
@Data
public abstract class AbstractWatermarkConfig implements Serializable {

    @Serial
    private static final long serialVersionUID = 7701002120099814530L;

    /**
     * 透明度
     */
    private Integer alpha;

    /**
     * 旋转角度
     */
    private Integer rotation;

    /**
     * 横向间距
     */
    private Integer latSpace;

    /**
     * 纵向间距
     */
    private Integer lonSpace;

}
