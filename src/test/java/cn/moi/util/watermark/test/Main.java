package cn.moi.util.watermark.test;

import cn.moi.util.watermark.config.IconWatermarkConfig;
import cn.moi.util.watermark.config.TextWatermarkConfig;
import cn.moi.util.watermark.utils.WatermarkUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("all")
public class Main {
    public static void main(String[] args) {
        TextWatermarkConfig textConfig = getTextWatermarkConfig();
        IconWatermarkConfig iconConfig = new IconWatermarkConfig();
        iconConfig.setAlpha(20);
        iconConfig.setRotation(0);
        iconConfig.setLatSpace(100);
        iconConfig.setLonSpace(100);
        iconConfig.setZoom(1.0);

        String desktop = "D:\\Him-m\\Desktop\\";

        File srcImgFile = new File(desktop + "1.pdf");
        File outImgFile = new File(desktop + "2.jpg");
        File iconFile = new File(desktop + "icon.png");
        File srcPdfFile = new File(desktop + "1.pdf");
        File outPdfFile = new File(desktop + "2.pdf");
        File srcWordFile = new File(desktop + "1.docx");
        File outWordFile = new File(desktop + "2.docx");
        File srcExcelFile = new File(desktop + "1.xlsx");
        File outExcelFile = new File(desktop + "2.xlsx");

//        WatermarkUtils.markImgWithText(srcImgFile, outImgFile, textConfig);
//        WatermarkUtils.markImgWithIcon(iconFile, srcImgFile, outImgFile, iconConfig);
        WatermarkUtils.markPdfWithText(srcPdfFile, outPdfFile, textConfig);
//        WatermarkUtils.markPdfWithIcon(iconFile, srcPdfFile, outPdfFile, iconConfig);
//        WatermarkUtils.markDocxWithText(srcWordFile, outWordFile, textConfig);
//        WatermarkUtils.markDocxWithImage(iconFile, srcWordFile, outWordFile, iconConfig);
//        WatermarkUtils.markExcelWithText(srcExcelFile, outExcelFile, textConfig);
//        WatermarkUtils.markExcelWithImage(iconFile, srcExcelFile, outExcelFile, iconConfig);
    }

    private static TextWatermarkConfig getTextWatermarkConfig() {
        List<Integer> color = new ArrayList<>();
        color.add(128);
        color.add(128);
        color.add(128);
        TextWatermarkConfig textConfig = new TextWatermarkConfig();
        textConfig.setText("测试用水印");
        textConfig.setFontFileName("SourceHanSansCN-Normal.ttf");
        textConfig.setAlpha(128);
        textConfig.setRotation(45);
        textConfig.setColor(color);
        textConfig.setFontName("思源黑体 CN Normal");
        textConfig.setFontSize(40);
        textConfig.setLatSpace(100);
        textConfig.setLonSpace(100);
        return textConfig;
    }
}